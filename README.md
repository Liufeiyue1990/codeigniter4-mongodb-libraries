# codeigniter4 mongodb libraries

#### 介绍
CodeIgniter 4 配置MongoDB数据库

#### 软件架构
软件架构说明


#### 安装教程

1.  下载app目录
2.  把app目录下的文件放入codeigniter框架中

#### 使用说明

1.  PHP7.0+
2.  CodeIgniter 4.0+
3.  MongoDB 3.0+ 推荐使用4.0+
4.  CoreModel.php 为自定义扩展核心类,UserModel继承CoreModel