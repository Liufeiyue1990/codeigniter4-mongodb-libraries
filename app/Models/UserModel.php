<?php

namespace App\Models;

use App\Core\CoreModel;
#use App\Libraries\MongoDB;

class UserModel extends CoreModel
{
    function __construct()
    {
        parent::__construct();
        #如要切换数据库，指定当前数据库活动对象
        #$this->mongo_db = new MongoDB(array('activate' => 'test'));
        $this->table_name = 'user';
        $this->table_field = array(
            'username' => '',
            'phone' => ''
        );
    }
}