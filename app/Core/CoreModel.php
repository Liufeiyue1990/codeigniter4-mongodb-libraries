<?php

namespace App\Core;

use App\Libraries\MongoDB;

class CoreModel
{
    protected $mongo_db;
    protected $table_name;
    protected $table_field;

    public function __construct()
    {
        $this->mongo_db = new MongoDB();
    }

    /**
     * 通用查询 单文档
     * @param null $criteria
     * @param null $orderBy
     * @return mixed|null
     * @throws \Exception
     */
    public function findOneBy($criteria = null, $orderBy = null)
    {
        if (is_array($criteria) && $criteria) {
            foreach ($criteria as $key => $val) {
                $this->mongo_db->where($key, $val);
            }
        }
        if (is_array($orderBy) && $orderBy) {
            $this->mongo_db->order_by($orderBy);
        } else {
            $this->mongo_db->order_by(array('_id' => 'DESC'));
        }
        $result = $this->mongo_db->find_one($this->table_name);
        if ($result) {
            return $result[0];
        }
        return null;
    }

    /**
     * 通用查询 多文档
     * @param null $criteria
     * @param null $limit
     * @param null $offset
     * @param null $select
     * @param null $orderBy
     * @return array|mixed|object
     * @throws \Exception
     */
    public function findBy($criteria = null, $limit = null, $offset = null, $select = null, $orderBy = null)
    {
        if (is_array($criteria) && $criteria) {
            foreach ($criteria as $key => $val) {
                $this->mongo_db->where($key, $val);
            }
        }
        if ($limit)
            $this->mongo_db->limit((int)$limit);
        if ($offset)
            $this->mongo_db->offset((int)$offset);
        if ($select)
            $this->mongo_db->select($select);
        if (is_array($orderBy) && $orderBy) {
            $this->mongo_db->order_by($orderBy);
        } else {
            $this->mongo_db->order_by(array('_id' => 'DESC'));
        }
        return $this->mongo_db->get($this->table_name);
    }
}